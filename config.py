import os

class Config(object):
    SECRET_KEY=os.environ.get("SECRET_KEY") or 'you_cant_guess_it'
    SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI") or 'postgresql://mohd:mohd@localhost:5432/ngo'
    print("SQL ALCHEMY", SQLALCHEMY_DATABASE_URI)
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CSRF_ENABLED = True