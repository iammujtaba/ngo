from app import app,checksum,db
from flask import render_template, redirect, request, url_for, logging, flash, session
from app.forms import DonateForm,LoginForm,RegisterForm
from random import randint
from flask_wtf.csrf import CsrfProtect
from app.models import User,Donate


MERCHANT_KEY='wkvh!Yoq3EUqN0oE'


@app.route('/')
def index():
    return render_template('index.html')

@app.route("/login",methods=["GET","POST"])
def login():
    form=LoginForm(request.form)
    if(request.method=="POST" and form.validate()):
        username=form.username.data
        password=form.password.data
        # print("FOMRMRMR",username,password)
        user=User.query.filter_by(username=username).first()
        if(user):
            print("PASSWORD",user.password_hash)
            if(password==user.password_hash):
                session['logged_in']=True
                session['username']=username
                session['user_id'] = user.id
                flash("You are logged in successfully..",'success')
                
                return redirect(url_for('index'))
                
            else:
                flash("Wrong Password.. Try Again...",'danger')
        else:
            flash("Username Does Not Exist In Database",'danger')
    return render_template('login.html',form=form)

@app.route("/logout")
def logout():
    session.clear()
    flash("You've been successfully logged out",'success')
    return redirect(url_for("login"))

@app.route("/register",methods=["GET","POST"])
def register():
    form=RegisterForm(request.form)
    if(request.method=="POST" and form.validate()):
        name=form.name.data
        email=form.email.data
        username=form.username.data
        password=form.password.data
        user_obj=User.query.filter_by(username=username).first()

        if(user_obj):
            flash("Username Already Taken...",'danger')
            return redirect(url_for("register"))
        email_obj=User.query.filter_by(email=email).first()        
        if(email_obj):
            flash("Email Already Register Kindly Login...",'danger')
            return redirect(url_for("login"))
        user=User(name=name,email=email,username=username,password_hash=password)
        db.session.add(user)
        db.session.commit()
        flash("Registration Successful Kindly Login...",'success')
        return redirect(url_for("login"))

    return render_template('register.html',form=form)

@app.route("/contact",methods=["GET","POST"])
def contact():
    return "Contact US PAGE"

donator_dict={}
@app.route("/donate",methods=["GET","POST"])
def donate():
    form=DonateForm(request.form)
    if(request.method=="POST" and form.validate()):

        donator_dict['name']=form.name.data
        donator_dict['phone']=form.phone.data
        donator_dict['address']=form.address.data
        donator_dict['amount']=form.amount.data
        donator_dict['email']=form.email.data
        donator_dict['order_id']=randint(10000,9999999999)
        
        param_dic={
            'MID': 'eABIcv17661333413077',
            'ORDER_ID': str(donator_dict['order_id']),
            'TXN_AMOUNT': str(donator_dict['amount']),
            'CUST_ID': str(donator_dict['email']),
            'INDUSTRY_TYPE_ID':'Retail',
            'WEBSITE':'WEBSTAGING',
            'CHANNEL_ID':'WEB',
	        'CALLBACK_URL':'http://127.0.0.1:5000/handlerequest',
        }
        param_dic['CHECKSUMHASH'] = checksum.generate_checksum(param_dic, MERCHANT_KEY)
        return render_template('paytm.html',param_dict=param_dic)
        
    return render_template('donate.html',form=form)

@app.route("/handlerequest",methods=["POST","GET"])
def handlerequest():
    # paytm will send you post request here
    if(request.method=="POST"):
        
        form=request.form
        print(form.items())
        for key,value in form.items():
            print(key,value)
        paytmChecksum = ""
        paytmParams = {}
        for key, value in form.items(): 
            if key == 'CHECKSUMHASH':
                paytmChecksum = value
            else:
                paytmParams[key] = value
        isValidChecksum = checksum.verify_checksum(paytmParams,MERCHANT_KEY, paytmChecksum)
        print(paytmParams)
        response=''
        if(isValidChecksum):
            global donator_dict
            if(paytmParams['RESPCODE'] == '01' and donator_dict):
                response='Your Transection is successful..Thanks! for donating'
                donate=Donate(name=donator_dict['name'],email=donator_dict['email'],phone=donator_dict['phone'],
                address=donator_dict['address'],order_id=donator_dict['order_id'],amount=donator_dict['amount']
                )
                db.session.add(donate)
                db.session.commit()
                donator_dict={}
                flash('Your Transection is successful..Thanks! for donating','success')
                return render_template('paymentstatus.html',response=response)
            else:
                response='Ops.. Internal Server Error or Payment Cancelled'
                flash('Ops...your transection is failed..Please try again ...','danger')
        return render_template('paymentstatus.html', response= paytmParams['RESPMSG'])
    return "<h1>Error 404 You are not authorized to use this page....</h1>"

    # Python Will handle post Request Sent by Paytm here.........



