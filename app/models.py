from app import db
# import date
from datetime import date
from sqlalchemy import DateTime,BigInteger
from datetime import datetime

class User(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.String(64), index=True)
    email = db.Column(db.String(120), index=True, unique=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    register_date = db.Column(db.DateTime, default=date.today())

    def __repr__(self):
        return '<User %s>'%self.username


class Donate(db.Model):
    id=db.Column(db.BigInteger,primary_key=True)
    name=db.Column(db.String(64),index=True)
    email=db.Column(db.String(264),index=True)
    phone=db.Column(db.BigInteger,index=True)
    address=db.Column(db.String(1000),index=True)
    order_id=db.Column(db.BigInteger,index=True)
    amount=db.Column(db.BigInteger,index=True)

    def __repr__(self):
        return '<User {}>'.format(self.name)