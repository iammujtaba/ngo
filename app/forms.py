from flask_wtf import FlaskForm
from wtforms import StringField,IntegerField,BooleanField,PasswordField,TextAreaField,SubmitField
from wtforms.validators import DataRequired, Length, Email, EqualTo

class DonateForm(FlaskForm):
    name=StringField("Name",validators=[DataRequired()])
    email=StringField("Email Id",validators=[DataRequired()])
    phone=StringField("Contact No",validators=[DataRequired()])
    address=TextAreaField("Address",validators=[DataRequired()])
    amount=IntegerField("Donation Amount",validators=[DataRequired()])
    submit=SubmitField("Donate Now")

class LoginForm(FlaskForm):
    username=StringField("Username",validators=[DataRequired()])
    password=PasswordField("Password",validators=[DataRequired()])
    submit=SubmitField("Login")

class RegisterForm(FlaskForm):
    name=StringField("Name",validators=[DataRequired()])
    email=StringField("Email",validators=[DataRequired()])
    username=StringField("Username",validators=[DataRequired()])
    password=PasswordField("Password",validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[
                                     DataRequired(), EqualTo('password')])
    submit=SubmitField("Register")




